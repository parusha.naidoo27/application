import React, { useState, useEffect } from "react";
import Axios from "axios";
import './manage.css';
export default function Manage() {

    const [setjobcardList, setJobCard] = useState([]);

    useEffect(() => {
        Axios.get('http://localhost:3001/api/get').then((response) => {
            console.log(response.data);
            setJobCard(response.data);
        })
    }, [])
    return (
        <div className="manage">
            <div className="card">
                <h2>Display</h2>

                <div className="table-container">
                    <table>
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Date</th>
                                <th>Employee Name</th>
                                <th>Product</th>
                                <th>From</th>
                                <th>Quantity</th>
                                <th>To</th>
                                <th>Opening Reading</th>
                                <th>Closing Reading</th>
                                <th>Total</th>
                                <th>Start Time</th>
                                <th>Finish Time</th>
                                <th>Comments</th>
                                <th>Manage</th>
                            </tr>
                        </thead>

                        <tbody>
                            {setjobcardList.map((val) => {
                                return (<tr key={val.ID}>
                                    <td>{val.ID}</td>
                                    <td>{val.Date}</td>
                                    <td>{val.Name}</td>
                                    <td>{val.Product}</td>
                                    <td>{val.Quantity}</td>
                                    <td>{val.FromLoc}</td>
                                    <td>{val.ToLoc}</td>
                                    <td>{val.OpenR}</td>
                                    <td>{val.CloseR}</td>
                                    <td>{val.Total}</td>
                                    <td>{val.ST}</td>
                                    <td>{val.FT}</td>
                                    <td>{val.Comments}</td>
                                </tr>)
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div >
    )
}
