import React, { useState, useEffect } from "react";
import Axios from "axios";
import './createjob.css'
function CreateJob() {

    const [name, setName] = useState(" ");
    const [date, setDate] = useState(" ");
    const [product, setProduct] = useState(" ");
    const [qty, setQty] = useState(0);
    const [fromloc, setFromLoc] = useState(" ");
    const [toloc, setToLoc] = useState(" ");
    const [openr, setOpenR] = useState(0);
    const [closer, setCloseR] = useState(0);
    const [total, setTotal] = useState(0);
    const [st, setSt] = useState(" ");
    const [ft, setFt] = useState(" ");
    const [comments, setComments] = useState(" ");

    //Handle submit
    const submitData = () => {
        Axios.post("http://localhost:3001/api/insert", {
            name: name,
            date: date,
            product: product,
            qty: qty,
            fromloc: fromloc,
            toloc: toloc,
            openr: openr,
            closer: closer,
            total: total,
            st: st,
            ft: ft,
            comments: comments,

        }).then(()=>{
            alert("successfully insert");
        })
    };

    return (
        <div className="createjob">
            <div className="card">
                <h2>Tank Farm Instruction Form</h2>

                <div className="row">

                    <div className="col">
                        <div className="form-group">
                            <label>Employee Name</label>
                            <input type="text" name="name" onChange={(e) => {
                                setName(e.target.value);
                            }} />
                        </div>
                    </div>

                    <div className="col">
                        <div className="form-group">
                            <label>Date</label>
                            <input type="date" name="date" onChange={(e) => {
                                setDate(e.target.value);
                            }} />
                        </div>
                    </div>

                    <div className="col">
                        <div className="form-group">
                            <label>Product</label>
                            <input type="text" name="product" onChange={(e) => {
                                setProduct(e.target.value);
                            }} />
                        </div>
                    </div>

                    <div className="col">
                        <div className="form-group">
                            <label>Quantity</label>
                            <input type="number" name="qty" onChange={(e) => {
                                setQty(e.target.value);
                            }} />
                        </div>
                    </div>

                    <div className="col">
                        <div className="form-group">
                            <label>From</label>
                            <input type="text" name="fromloc" onChange={(e) => {
                                setFromLoc(e.target.value);
                            }} />
                        </div>
                    </div>

                    <div className="col">
                        <div className="form-group">
                            <label>To</label>
                            <input type="text" name="toloc" onChange={(e) => {
                                setToLoc(e.target.value);
                            }} />
                        </div>
                    </div>

                    <div className="col">
                        <div className="form-group">
                            <label>Opening Reading</label>
                            <input type="number" name="opene" onChange={(e) => {
                                setOpenR(e.target.value);
                            }} />
                        </div>
                    </div>

                    <div className="col">
                        <div className="form-group">
                            <label>Closing Reading</label>
                            <input type="number" name="closer" onChange={(e) => {
                                setCloseR(e.target.value);
                            }} />
                        </div>
                    </div>

                    <div className="col">
                        <div className="form-group">
                            <label>Total</label>
                            <input type="number" name="total" onChange={(e) => {
                                setTotal(e.target.value);
                            }} />
                        </div>
                    </div>

                    <div className="col">
                        <div className="form-group">
                            <label>Start Time</label>
                            <input type="time" name="st" onChange={(e) => {
                                setSt(e.target.value);
                            }} />
                        </div>
                    </div>
                    <div className="col">
                        <div className="form-group">
                            <label>Finish Time</label>
                            <input type="time" name="ft" onChange={(e) => {
                                setFt(e.target.value);
                            }} />
                        </div>
                    </div>

                    <div className="col">
                        <div className="form-group">
                            <label>Comments</label>
                            <textarea rows="4" cols="50" name="comments" onChange={(e) => {
                                setComments(e.target.value);
                            }}></textarea>
                        </div>
                    </div>

                    <div className="col">
                        <input type="submit" value="Create" onClick={submitData} />
                    </div>

                </div>
            </div>
        </div>
    )

}

export default CreateJob;