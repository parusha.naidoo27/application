import "./App.css";
import * as React from "react";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import HomeIcon from "@mui/icons-material/Home";
import Create from "@mui/icons-material/Create";
import { Work } from "@material-ui/icons";
import AssessmentIcon from "@mui/icons-material/Assessment";
import MenuItem from "@mui/material/MenuItem";
import PopupState, { bindTrigger, bindMenu } from "material-ui-popup-state";
import { AppBar, Toolbar, IconButton, Typography } from "@material-ui/core";
import { Link, Route, BrowserRouter as Router } from "react-router-dom";
import Home from "./pages/Home";
import CreateJob from "./pages/CreateJob";
import Manage from "./pages/Manage";
import Reports from "./pages/Reports";

function App() {
  const linkStyle = {
    color: "grey",
    textDecoration: "none",
  };
  return (
    <Router>
      <div className="App">
        <AppBar style={{ backgroundColor: "green" }}>
          <Toolbar>
            <IconButton aria-label="app" color="inherit">
            </IconButton>
            <PopupState variant="popover" popupId="demo-popup-menu">
              {(popupState) => (
                <React.Fragment>
                  <Button
                    variant="contained"
                    {...bindTrigger(popupState)}
                    style={{
                      backgroundColor: "transparent",
                      marginLeft: "-30px",
                    }}
                  >
                    <MenuIcon />
                  </Button>

                  <Menu {...bindMenu(popupState)}>
                    <MenuItem
                      onClick={popupState.close}
                      style={{ color: "grey" }}
                    >
                      <HomeIcon style={{ paddingRight: "3px" }} />
                      <Link to="/" exact ="true" style={linkStyle}>
                        {" "}
                        Dashboard
                      </Link>
                    </MenuItem>

                    <MenuItem
                      onClick={popupState.close}
                      style={{ color: "grey" }}
                    >
                      <Create style={{ paddingRight: "3px" }} />
                      <Link to="/createjob"  exact="true" style={linkStyle}>
                        Create Job Task
                      </Link>
                    </MenuItem>

                     <MenuItem
                      onClick={popupState.close}
                      style={{ color: "grey" }}
                    >
                      <Work style={{ paddingRight: "3px" }} />
                      <Link to="/manage"  exact="true" style={linkStyle}>
                        {" "}
                        Manage
                      </Link>
                    </MenuItem>
                    
                    <MenuItem
                      onClick={popupState.close}
                      style={{ color: "grey" }}
                    >
                      <AssessmentIcon style={{ paddingRight: "3px" }} />
                      <Link to="/reports"  exact="true" style={linkStyle}>
                        {" "}
                        Reports
                      </Link>
                    </MenuItem>
                  </Menu>
                </React.Fragment>
              )}
            </PopupState>
            <div className="logo">
              <Typography variant="h6"> JAG </Typography>
            </div>
          </Toolbar>
        </AppBar>
      </div>
      <Route exact path="/" component={Home} />
      <Route path="/createjob" component={CreateJob} />
      <Route path="/manage" component={Manage} />
      <Route path="/reports" component={Reports} />
    </Router>
  );
}

export default App;
