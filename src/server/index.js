const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const cors = require("cors");
const mysql = require("mysql");
const PORT = process.env.PORT || 3001;

const db = mysql.createPool({
  host: "sql6.freesqldatabase.com",
  user: "sql6446988",
  password: "V4qKTxwjEa",
  database: "sql6446988",
});

app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/api/get", (req, res) => {
  const sqlSelect = "SELECT * from JobCard";
  db.query(sqlSelect, (err, result) => {
    res.send(result);
  });
});

app.post("/api/insert", (req, res) => {
  console.log(req.body);
  const name = req.body.name;
  const date = req.body.date;
  const product = req.body.product;
  const qty = req.body.qty;
  const fromloc = req.body.fromloc;
  const toloc = req.body.toloc;
  const openr = req.body.openr;
  const closer = req.body.closer;
  const total = req.body.total;
  const st = req.body.st;
  const ft = req.body.ft;
  const comments = req.body.comments;

  const sqlInsert =
    "INSERT INTO JobCard (Name,Date,Product,Quantity,FromLoc,ToLoc,OpenR,CloseR,Total,ST,FT,Comments) Values(?,?,?,?,?,?,?,?,?,?,?,?)";

  db.query(
    sqlInsert,
    [
      name,
      date,
      product,
      qty,
      fromloc,
      toloc,
      openr,
      closer,
      total,
      st,
      ft,
      comments,
    ],
    (err, result) => {
      console.log(err);
    }
  );
});

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});
